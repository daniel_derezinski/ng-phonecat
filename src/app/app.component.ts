import { Component } from '@angular/core';

@Component({
  selector: 'pca-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ng-phonecat';
}
