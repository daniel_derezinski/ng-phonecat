import {
  ComponentFactoryResolver,
  Directive,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewContainerRef
} from '@angular/core';
import { BaseComponent } from '../../shared/base/base-component';
import { DetailComponent } from '../detail/detail.component';
import { Observable } from 'rxjs';
import { IPhoneDetail } from '../../shared/contracts/phones';
import { take, takeUntil } from 'rxjs/operators';
import { DetailType, detailTypeMap, IDetail, IDetailInputs, IDetailOutputs } from '../common/detail';
import { BaseDetail } from '../common/base-detail';

@Directive({
  selector: '[pcaDynamicDetails]'
})
export class DynamicDetailsDirective extends BaseComponent implements OnChanges {

  // UWAGA inputy kompnentu szczegułów oraz dyrektywa powinna działać na Observable - teraz jeżeli zmienni sie
  // mainImageUrl to komponent jest ponownie tworzony i wstawiany. Jeżli będą to observable to zmienią się one tylko raz i referencje
  // nie będą się zmieniać. Zmiany będą tylko wtedy kiedy dojdzie do zmiany w typie.
  @Input() mainImageUrl: string;
  @Input() phone: IPhoneDetail;
  @Output() setMainImage = new EventEmitter<string>();
  @Input('pcaDynamicDetails') type: DetailType;

  constructor(private viewContainerRef: ViewContainerRef,
              private componentFactoryResolver: ComponentFactoryResolver) {
    super();
  }

  ngOnChanges(changes: SimpleChanges) {
    this.loadComponent();
  }

  loadComponent() {
    this.viewContainerRef.clear();
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(detailTypeMap[this.type]);
    const componentRef = this.viewContainerRef.createComponent(componentFactory);
    const componentInstance: BaseDetail = componentRef.instance;

    componentInstance.phone = this.phone;
    componentInstance.mainImageUrl = this.mainImageUrl;

    componentInstance.setMainImage
      .pipe(
        takeUntil(this.unSubscribe),
      )
      .subscribe((url: string) => {
        this.setMainImage.emit(url);
      })

    // Object.keys(this.inputsConfig).forEach((option) => {
    //   componentInstance[option] = this.inputsConfig[option];
    // });
    //
    // Object.keys(this.outputsConfig).forEach((option) => {
    //   componentInstance[option]
    //     .pipe(
    //       takeUntil(this.unSubscribe)
    //     )
    //     .subscribe((data: any) => {
    //       this.outputsConfig[option](data);
    //     })
    // })


  }

}
