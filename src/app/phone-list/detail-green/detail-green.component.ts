import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IPhoneDetail } from '../../shared/contracts/phones';
import { IDetail } from '../common/detail';
import { BaseDetail } from '../common/base-detail';

@Component({
  selector: 'pca-detail-green',
  templateUrl: './detail-green.component.html',
  styleUrls: ['./detail-green.component.scss']
})
export class DetailGreenComponent extends BaseDetail {

}
