import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailGreenComponent } from './detail-green.component';

describe('DetailGreenComponent', () => {
  let component: DetailGreenComponent;
  let fixture: ComponentFixture<DetailGreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailGreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailGreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
