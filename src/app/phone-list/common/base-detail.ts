import { EventEmitter, Input, Output } from '@angular/core';
import { IPhoneDetail } from '../../shared/contracts/phones';
import { IDetail } from './detail';

export abstract class BaseDetail implements IDetail {

  @Input() mainImageUrl: string;
  @Input() phone: IPhoneDetail;
  @Output() setMainImage = new EventEmitter<string>();

  onSetImage(img: string) {
    this.setMainImage.emit(img);
  }
}
