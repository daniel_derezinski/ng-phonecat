import { IPhoneDetail } from '../../shared/contracts/phones';
import { EventEmitter, Type } from '@angular/core';
import { DetailComponent } from '../detail/detail.component';
import { DetailGreenComponent } from '../detail-green/detail-green.component';
import { DetailRedComponent } from '../detail-red/detail-red.component';
import { BaseDetail } from './base-detail';
import { DetailBlueComponent } from '../detail-blue/detail-blue.component';

export interface IDetail {
  mainImageUrl: string;
  phone: IPhoneDetail;
  setMainImage: EventEmitter<string>;

  onSetImage(img: string): void;
}

export enum DetailType {
  NORMAL = 'NORMAL',
  GREEN = 'GREEN',
  RED = 'RED',
  BLUE = 'BLUE',
}

export const detailTypeMap: {[key in DetailType]: Type<BaseDetail>} = {
  [DetailType.NORMAL]: DetailComponent,
  [DetailType.GREEN]: DetailGreenComponent,
  [DetailType.RED]: DetailRedComponent,
  [DetailType.BLUE]: DetailBlueComponent,
};

export interface IDetailInputs {
  phone: IPhoneDetail;
  mainImageUrl: string;
}

export interface IDetailOutputs {
  setMainImage (url: string): void;
}
