export interface IPhoneListFilters {
  search?: string;
  order?: 'age' | 'sortName';
}
