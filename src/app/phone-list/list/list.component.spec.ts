import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListComponent } from './list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { IHttpService, IPhone, IPhoneDetail } from '../../shared/contracts/phones';
import { Observable } from 'rxjs';
import { HttpService } from '../../shared/services/http.service';
import { Store, StoreModule } from '@ngrx/store';
import * as phoneListReducers from '../store/reducers';
import * as rootReducers from '../../store/reducers';
import * as rootActions from '../../store/actions';
import { cold } from 'jasmine-marbles';
import * as phoneListActions from '../store/actions';
import Spy = jasmine.Spy;

class HttpServiceStub implements IHttpService {
  getInstitutions(): Observable<any> {
    return undefined;
  }

  getPhone(id: string): Observable<IPhoneDetail> {
    return undefined;
  }

  getPhoneList(): Observable<IPhone[]> {
    return undefined;
  }
}

describe('ListComponent', () => {
  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;
  let store: Store<any>;
  let storeDispatchSpy: Spy;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        RouterTestingModule,
        StoreModule.forRoot({
          ...rootReducers.reducers,
          'phoneList': phoneListReducers.reducers,
        })
      ],
      declarations: [ ListComponent ],
      providers: [{provide: HttpService, useClass: HttpServiceStub}]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListComponent);
    component = fixture.componentInstance;
    store = TestBed.get(Store);
    storeDispatchSpy = spyOn(store, 'dispatch').and.callThrough();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should define search from', () => {
    expect(component.searchForm).toBeTruthy()
  });

  it('should define correct search form', () => {
    const result = component.searchForm.value;

    expect(result).toEqual({
      search: '',
      order: 'sortName'
    })
  });

  it('should define list stream with correct data', () => {
    const list: IPhone[] = [
      {
        "age": 0,
        "id": "motorola-xoom-one",
        "imageUrl": "img/phones/motorola-xoom-one.0.jpg",
        "name": "Motorola XOOM One",
        "snippet": "Snippet XOOM One"
      },
      {
        "age": 1,
        "id": "motorola-xoom",
        "imageUrl": "img/phones/motorola-xoom.0.jpg",
        "name": "MOTOROLA XOOM",
        "snippet": "Snippet XOOM"
      },
    ];
    const expected = cold('a', {a: list});

    store.dispatch(new phoneListActions.StoreFetchedList(list));

    expect(component.list$).toBeObservable(expected)
  });

  it('should define show spinner stream with correct data', () => {
    const expected = cold('a', {a: true});

    store.dispatch(new rootActions.StartProgress());

    expect(component.showSpinner$).toBeObservable(expected);
  });

  it('should dispatch FetchList action on component init', () => {
    expect(storeDispatchSpy).toHaveBeenCalledWith(new phoneListActions.FetchList());
  });

  it('should patch form values on component init only once', () => {
    const spy = spyOn(component.searchForm, 'patchValue');

    component.ngOnInit();

    expect(spy).toHaveBeenCalledWith({
      search: '',
      order: 'sortName'
    });
    expect(component.isPatching).toBeFalsy();

    store.dispatch(new phoneListActions.StorePhoneListFilters({
      search: 'test',
      order: 'age',
    }));

    spy.calls.reset();

    expect(spy).not.toHaveBeenCalled();

  });


});
