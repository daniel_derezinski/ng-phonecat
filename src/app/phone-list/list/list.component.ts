import { Component, Inject, OnInit } from '@angular/core';
import { IPhone } from '../../shared/contracts/phones';
import { EMPTY, merge, Observable, of, throwError } from 'rxjs';
import { BaseComponent } from '../../shared/base/base-component';
import { catchError, debounceTime, filter, map, take, tap } from 'rxjs/operators';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as phoneListReducers from '../store/reducers';
import * as phoneListActions from '../store/actions';
import * as phoneListSelectors from '../store/selectors';
import * as rootSelectors from '../../store/selectors';
import { select, Store } from '@ngrx/store';
import { IPhoneListFilters } from '../common/filters';
import { HttpService } from '../../shared/services/http.service';
import { HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { MY_TOKEN, WINDOW } from '../../shared/shared.module';

@Component({
  selector: 'pca-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent extends BaseComponent implements OnInit {

  list$: Observable<IPhone[]>;

  searchForm: FormGroup;

  showSpinner$: Observable<boolean>;

  isPatching = false;

  errorMessage: string;

  idx = 1;

  constructor(private formBuilder: FormBuilder,
              private httpService: HttpService,
              private store: Store<phoneListReducers.IRootState>) {
    super();
    this.searchForm = this.formBuilder.group({
      search: [''],
      order: ['sortName']
    });
  }

  ngOnInit() {

    this.list$ = this.store.pipe(select(phoneListSelectors.selectFilteredPhoneList));
    this.showSpinner$ = this.store.pipe(select(rootSelectors.selectShowProgressState));
    this.store.dispatch(new phoneListActions.FetchList());

    this.store.pipe(
      select(phoneListSelectors.selectPhoneListFilter),
      tap(() => {
        this.isPatching = true;
      }),
      take(1),
    )
      .subscribe((filters: IPhoneListFilters) => {
        this.searchForm.patchValue(filters);
        this.isPatching = false;
      });


    merge(
      this.searchForm.get('search').valueChanges.pipe(debounceTime(500), map(val => ({ search: val }))),
      this.searchForm.get('order').valueChanges.pipe(map(val => ({ order: val }))),
    )
      .pipe(filter(() => !this.isPatching))
      .subscribe((filters: IPhoneListFilters) => {
        this.store.dispatch(new phoneListActions.StorePhoneListFilters(filters));
      });

  }

  fetchList() {
    this.errorMessage = '';
    this.httpService.getInstitutions()
      .pipe(
        catchError((error: HttpErrorResponse) => {
          this.errorMessage = error.message;
          return of(['asdasd', 'asdasd']);
        }),
        map((response) => {
          console.log('idx', this.idx, response);
          const result = [{...response[this.idx]}];
          this.idx = this.idx + 1;
          return result;
        })
      )
      .subscribe((response) => {
          console.log('next', response);
        },
        (message: string) => {
          console.log('error', message);

        },
        () => {
          console.log('complete');
        });
  }
}
