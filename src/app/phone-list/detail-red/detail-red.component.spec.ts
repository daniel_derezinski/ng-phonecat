import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailRedComponent } from './detail-red.component';

describe('DetailRedComponent', () => {
  let component: DetailRedComponent;
  let fixture: ComponentFixture<DetailRedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailRedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailRedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
