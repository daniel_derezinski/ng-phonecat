import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { IPhoneDetail } from '../../shared/contracts/phones';
import { IDetail } from '../common/detail';
import { BaseDetail } from '../common/base-detail';

@Component({
  selector: 'pca-detail-red',
  templateUrl: './detail-red.component.html',
  styleUrls: ['./detail-red.component.scss']
})
export class DetailRedComponent extends BaseDetail{
}
