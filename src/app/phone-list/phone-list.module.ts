import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PhoneListRoutingModule } from './phone-list-routing.module';
import { SharedModule } from '../shared/shared.module';
import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DetailPageComponent } from './detail-page/detail-page.component';
import { StoreModule } from '@ngrx/store';
import * as phoneListReducers from './store/reducers';
import { EffectsModule } from '@ngrx/effects';
import { PhoneListEffects } from './store/effects';
import { DynamicDetailsDirective } from './directives/dynamic-details.directive';
import { DetailGreenComponent } from './detail-green/detail-green.component';
import { DetailRedComponent } from './detail-red/detail-red.component';
import { DetailBlueComponent } from './detail-blue/detail-blue.component';

@NgModule({
  imports: [
    CommonModule,
    PhoneListRoutingModule,
    SharedModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    StoreModule.forFeature('phoneList', phoneListReducers.reducers),
    EffectsModule.forFeature([PhoneListEffects]),
  ],
  declarations: [ListComponent, DetailComponent, DetailPageComponent, DynamicDetailsDirective, DetailGreenComponent, DetailRedComponent, DetailBlueComponent],
  entryComponents: [
    DetailComponent,
    DetailRedComponent,
    DetailGreenComponent,
    DetailBlueComponent,
  ]
})
export class PhoneListModule { }
