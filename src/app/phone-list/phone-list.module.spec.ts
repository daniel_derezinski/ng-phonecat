import { PhoneListModule } from './phone-list.module';

describe('PhoneListModule', () => {
  let phoneListModule: PhoneListModule;

  beforeEach(() => {
    phoneListModule = new PhoneListModule();
  });

  it('should create an instance', () => {
    expect(phoneListModule).toBeTruthy();
  });
});
