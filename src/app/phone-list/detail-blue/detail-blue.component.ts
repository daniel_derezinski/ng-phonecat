import { Component, OnInit } from '@angular/core';
import { BaseDetail } from '../common/base-detail';

@Component({
  selector: 'pca-detail-blue',
  templateUrl: './detail-blue.component.html',
  styleUrls: ['./detail-blue.component.scss']
})
export class DetailBlueComponent extends BaseDetail{
}
