import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailBlueComponent } from './detail-blue.component';

describe('DetailBlueComponent', () => {
  let component: DetailBlueComponent;
  let fixture: ComponentFixture<DetailBlueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailBlueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailBlueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
