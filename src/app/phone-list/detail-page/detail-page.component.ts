import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { IPhoneDetail } from '../../shared/contracts/phones';
import { ActivatedRoute, Params } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { BaseComponent } from '../../shared/base/base-component';
import * as phoneListReducers from '../store/reducers';
import * as phoneListActions from '../store/actions';
import * as phoneListSelectors from '../store/selectors';
import { select, Store } from '@ngrx/store';
import { DetailType, IDetailInputs, IDetailOutputs } from '../common/detail';


@Component({
  selector: 'pca-detail-page',
  templateUrl: './detail-page.component.html',
})
export class DetailPageComponent extends BaseComponent implements OnInit {

  mainImageUrl$: Observable<string>;
  phone$: Observable<IPhoneDetail>;

  phone: IPhoneDetail = null;
  mainImageUrl: string = '';

  detailType = DetailType;

  detailInputsConfig: IDetailInputs = {
    mainImageUrl: '',
    phone: null,
  };
  detailOutputsConfig: IDetailOutputs = {
    setMainImage (url: string) {
      this.setMainImage(url);
    }
  };


  private typeDetail = new BehaviorSubject<DetailType>(DetailType.NORMAL);
  typeDetail$ = this.typeDetail.asObservable();

  constructor(private route: ActivatedRoute,
              private store: Store<phoneListReducers.IRootState>) {
    super();
  }

  ngOnInit() {
    this.phone$ = this.store.pipe(select(phoneListSelectors.selectPhoneDetail));
    this.mainImageUrl$ = this.store.pipe(select(phoneListSelectors.selectPhoneMainImageURL));

    this.phone$
      .pipe(
        takeUntil(this.unSubscribe)
      )
      .subscribe((phone) => {
        this.phone = phone;
        this.detailInputsConfig = {
          ...this.detailInputsConfig,
          phone: phone,
        }
      });

    this.mainImageUrl$
      .pipe(
        takeUntil(this.unSubscribe)
      )
      .subscribe((mainImageUrl) => {
        this.mainImageUrl = mainImageUrl;
        this.detailInputsConfig = {
          ...this.detailInputsConfig,
          mainImageUrl: mainImageUrl,
        }
      });

    this.route.params
      .pipe(
        takeUntil(this.unSubscribe)
      )
      .subscribe((params: Params) => {
        console.log('Phone id', params['id']);
        this.store.dispatch(new phoneListActions.FetchPhoneDetail(params['id']));
      });
  }

  setImage(img: string) {
    this.store.dispatch(new phoneListActions.StoreMainImgUrl(img))
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    // this.httpService.resetPhone();
  }

  setDetail(type: DetailType) {
    this.typeDetail.next(type);
  }
}
