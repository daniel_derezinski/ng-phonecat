import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { EMPTY, from, Observable, of, throwError } from 'rxjs';
import { Action, Store } from '@ngrx/store';
import * as phoneListActions from './actions';
import { catchError, exhaustMap, finalize, map, mergeMap, switchMap, tap } from 'rxjs/operators';
import { HttpService } from '../../shared/services/http.service';
import { IPhone, IPhoneDetail } from '../../shared/contracts/phones';
import * as phoneListReducers from './reducers';
import * as rootActions from '../../store/actions';
import { HttpErrorResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

@Injectable()
export class PhoneListEffects {

  constructor(private actions$: Actions,
              private httpService: HttpService,
              private store: Store<phoneListReducers.IRootState>,
              private router: Router,
              private route: ActivatedRoute) {}
  //
  @Effect()
  phoneList$ = this.actions$
    .pipe(
      ofType(phoneListActions.FETCH_LIST),
      tap(() => {
        this.store.dispatch(new rootActions.StartProgress())
      }),
      exhaustMap(() => {
        return this.httpService.getPhoneList()
          .pipe(
            map((response) => {
              return response.map((ele) => {
                return {
                  ...ele,
                  sortName: ele.name.toLowerCase(),
                }
              });
            }),
            catchError((errorResponse: HttpErrorResponse) => {
              if (errorResponse.status === 404) {
                console.warn('404');
                this.router.navigateByUrl('/404');
                return EMPTY;
              }
              return throwError(errorResponse);
            }),
            finalize(() => {
              this.store.dispatch(new rootActions.StopProgress());
            })
          );
      }),
      mergeMap((response: IPhone[]) => {
        return from([
          new phoneListActions.StoreFetchedList(response),
        ])
      })
    );

  @Effect()
  phoneDetail$ = this.actions$
    .pipe(
      ofType(phoneListActions.FETCH_PHONE_DETAIL),
      map(action => (<phoneListActions.FetchPhoneDetail>action).payload),
      tap(() => {
        this.store.dispatch(new rootActions.StartProgress())
      }),
      mergeMap((payload) => {
        return this.httpService.getPhone(payload);
      }),
      mergeMap((response: IPhoneDetail) => {
        return from([
          new phoneListActions.StoreFetchedPhoneDetail(response),
          new rootActions.StopProgress(),
        ]);
      })
    );

  @Effect({dispatch: false})
  getInstitutions$ = this.actions$
    .pipe(
      ofType(phoneListActions.FETCH_LIST),
      exhaustMap(() => {
        return this.httpService.getInstitutions()
      })

    )
}
