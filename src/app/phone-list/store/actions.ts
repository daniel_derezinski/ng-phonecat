import { Action } from '@ngrx/store';
import { IPhone, IPhoneDetail } from '../../shared/contracts/phones';
import { IPhoneListFilters } from '../common/filters';

export const FETCH_LIST = '[phone-list] Fetch phone list';

export class FetchList implements Action {
  type = FETCH_LIST;
}

export const STORE_FETCHED_LIST = '[phone-list] Store fetched list';

export class StoreFetchedList implements Action {
  type = STORE_FETCHED_LIST;

  constructor(public payload: IPhone[]) {
  }
}

export const FETCH_PHONE_DETAIL = '[phone-list] fetch phone detail';

export class FetchPhoneDetail implements Action {
  type = FETCH_PHONE_DETAIL;

  constructor(public payload: string) {
  }
}

export const STORE_FETCHED_PHONE_DETAIL = '[phone-list] store fetched phone detail';

export class StoreFetchedPhoneDetail implements Action {
  type = STORE_FETCHED_PHONE_DETAIL;

  constructor(public payload: IPhoneDetail) {
  }
}

export const STORE_PHONE_LIST_FILTERS = '[phone-list] store phone list filters';

export class StorePhoneListFilters implements Action {
  type = STORE_PHONE_LIST_FILTERS;

  constructor(public payload: IPhoneListFilters) {}
}

export const STORE_MAIN_IMG_URL = '[phone-list] store main img url on phone detail';

export class StoreMainImgUrl implements Action {
  type = STORE_MAIN_IMG_URL;

  constructor(public payload: string) {}
}

export type All =
  FetchList |
  StoreFetchedList |
  FetchPhoneDetail |
  StoreFetchedPhoneDetail |
  StorePhoneListFilters |
  StoreMainImgUrl;
