import { IPhone, IPhoneDetail } from '../../shared/contracts/phones';
import * as rootReducers from '../../store/reducers';
import * as phoneListActions from './actions';
import { IPhoneListFilters } from '../common/filters';

export interface IPhoneListState {
  list: IPhone[];
  detail: {
    mainImageURL: string;
    phone: IPhoneDetail;
  };
  filters: IPhoneListFilters;
}

const initialState: IPhoneListState = {
  list: [],
  detail: {
    mainImageURL: '',
    phone: null,
  },
  filters: {
    search: '',
    order: 'sortName',
  }
};

export interface IRootState extends rootReducers.IRootState {
  phoneList: IPhoneListState,
}

export function reducers(state: IPhoneListState = initialState, action: phoneListActions.All) {
  switch (action.type) {
    case phoneListActions.STORE_FETCHED_LIST: {
      return <IPhoneListState> {
        ...state,
        list: [
          ...(<phoneListActions.StoreFetchedList>action).payload,
        ]
      };
    }

    case phoneListActions.STORE_FETCHED_PHONE_DETAIL: {
      const payload = (<phoneListActions.StoreFetchedPhoneDetail>action).payload;
      return <IPhoneListState> {
        ...state,
        detail: {
          ...state.detail,
          mainImageURL: payload.images[0],
          phone: {
            ...payload
          }
        }
      }
    }

    case phoneListActions.STORE_PHONE_LIST_FILTERS: {
      const payload = (<phoneListActions.StorePhoneListFilters>action).payload;
      return <IPhoneListState> {
        ...state,
        filters: {
          ...state.filters,
          ...payload
        }
      };
    }

    case phoneListActions.STORE_MAIN_IMG_URL: {
      const payload = (<phoneListActions.StoreMainImgUrl>action).payload;
      return <IPhoneListState> {
        ...state,
        detail: {
          ...state.detail,
          mainImageURL: payload
        }
      };
    }

    default:
      return state;
  }
}
