import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as phoneListReducers from './reducers';
import { IPhone } from '../../shared/contracts/phones';
import { IPhoneListFilters } from '../common/filters';
import { sortBy as _sortBy } from 'lodash';

const selectFeature = createFeatureSelector<phoneListReducers.IRootState, phoneListReducers.IPhoneListState>(
  'phoneList'
);

export const selectPhoneListFilter = createSelector(
  selectFeature,
  (state: phoneListReducers.IPhoneListState): IPhoneListFilters => {
    return state.filters;
  }
);

export const selectPhoneList = createSelector(
  selectFeature,
  (state: phoneListReducers.IPhoneListState) => {
    return state.list;
  }
);


export const selectFilteredPhoneList = createSelector(
  selectPhoneList,
  selectPhoneListFilter,
  (list: IPhone[], filters: IPhoneListFilters): IPhone[] => {
    let newList: IPhone[] = list.filter((ele: IPhone, idx) => {
      if (filters.search) {
        return ele.name.toLowerCase().includes(filters.search.toLowerCase());
      }
      return true;
    });

    newList = _sortBy(newList, filters.order);

    return newList;
  }
);

export const selectPhoneDetail = createSelector(
  selectFeature,
  (state: phoneListReducers.IPhoneListState) => {
    return state.detail.phone;
  }
);

export const selectPhoneMainImageURL = createSelector(
  selectFeature,
  (state: phoneListReducers.IPhoneListState) => {
    if (state.detail.mainImageURL) {
      return `assets/${state.detail.mainImageURL}`;
    }
    return '';
  }
);
