import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { IPhoneDetail } from '../../shared/contracts/phones';
import { IDetail } from '../common/detail';
import { BaseDetail } from '../common/base-detail';

@Component({
  selector: 'pca-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
})
export class DetailComponent extends BaseDetail {
}
