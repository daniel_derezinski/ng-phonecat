import { Action } from '@ngrx/store';

export const START_PROGRESS = '[root] start progress';

export class StartProgress implements Action {
  readonly type = START_PROGRESS;
}

export const STOP_PROGRESS = '[root] stop progress';

export class StopProgress implements Action {
  readonly type = STOP_PROGRESS;
}

export type All =
  StartProgress |
  StopProgress;
