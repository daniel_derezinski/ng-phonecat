import * as rootActions from './actions';
import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { storeFreeze } from 'ngrx-store-freeze';
import { environment } from '../../environments/environment';

export interface ILayoutState {
  showProgress: number;
}

export interface IRootState {
  layout: ILayoutState;
}

const layoutInitialState: ILayoutState = {
  showProgress: 0,
};

export const reducers: ActionReducerMap<IRootState> = {
  layout: layoutReducer,
};

export const metaReducers: MetaReducer<any>[] = !environment.production ? [storeFreeze] : [];

export function layoutReducer(state: ILayoutState = layoutInitialState, action: rootActions.All) {
  switch(action.type) {
    case rootActions.START_PROGRESS: {
      return {
        ...state,
        showProgress: state.showProgress + 1,
      }
    }

    case rootActions.STOP_PROGRESS: {
      return {
        ...state,
        showProgress: state.showProgress - 1 <= 0 ? 0 : state.showProgress - 1,
      }
    }

    default:
      return state;
  }


}

