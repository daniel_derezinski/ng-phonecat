import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ILayoutState, IRootState } from './reducers';

const selectLayout = createFeatureSelector<IRootState, ILayoutState>(
  'layout'
);

export const selectShowProgressState = createSelector(
  selectLayout,
  (state: ILayoutState) => {
    return !!state.showProgress;
  }
)
