// export class Phone {
//   age: number;
//   id: string;
//   imageUrl: string;
//   name: string;
//   snippet: string;
// }
//
// export type PhoneType = typeof Phone;

import { Observable } from 'rxjs';

export interface IPhone {
  age: number;
  id: string;
  imageUrl: string;
  name: string;
  snippet: string;
}

export class Phone implements IPhone {
  age: number;
  id: string;
  imageUrl: string;
  name: string;
  snippet: string;
  sortName?: string;
}

export interface IPhoneDetail {
  mainImage?: string;
  "additionalFeatures": string;
  "android": {
    "os": string;
    "ui": string;
  },
  "availability": string[];
  "battery": {
    "standbyTime": string;
    "talkTime": string;
    "type": string;
  },
  "camera": {
    "features": string[];
    "primary": string;
  },
  "connectivity": {
    "bluetooth": string;
    "cell": string;
    "gps": boolean;
    "infrared": boolean;
    "wifi": string;
  },
  "description": string;
  "display": {
    "screenResolution": string;
    "screenSize": string;
    "touchScreen": boolean;
  },
  "hardware": {
    "accelerometer": boolean;
    "audioJack": string;
    "cpu": string;
    "fmRadio": boolean;
    "physicalKeyboard": boolean;
    "usb": string;
  },
  "id": string;
  "images": string[];
  "name": string;
  "sizeAndWeight": {
    "dimensions": string[];
    "weight": string;
  },
  "storage": {
    "flash": string;
    "ram": string;
  }
}

export interface IHttpService {
  getPhoneList(): Observable<IPhone[]>;
  getPhone(id: string): Observable<IPhoneDetail>;
  getInstitutions(): Observable<any>
}
