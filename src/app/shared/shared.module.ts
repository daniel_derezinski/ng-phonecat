import { InjectionToken, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpService } from './services/http.service';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CheckmarkPipe } from './pipes/checkmark.pipe';
import { NotFoundComponent } from './not-found/not-found.component';
import { SpinnerComponent } from './spinner/spinner.component';
import { environment } from '../../environments/environment';
import { HttpSecondService } from './services/http-second.service';
import { httpServiceFactory } from './services/service-factory';

export const MY_TOKEN = new InjectionToken('MY_TOKEN');
export const WINDOW = new InjectionToken('WINDOW');

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  declarations: [CheckmarkPipe, NotFoundComponent, SpinnerComponent],
  providers: [
    // HttpService,
    // {provide: HttpService, useClass: environment.services.phoneList ?  HttpSecondService : HttpService},
    { provide: HttpService, deps: [HttpClient], useFactory: httpServiceFactory },
    { provide: MY_TOKEN, useClass: HttpSecondService },
    { provide: WINDOW, useValue: window},
    // {
    //   provide: MY_TOKEN, deps: [HttpService, HttpSecondService], useFactory: (...deps: HttpService[]) => {
    //     return deps;
    //   }
    // },
  ],
  exports: [CheckmarkPipe, NotFoundComponent, SpinnerComponent],
})
export class SharedModule {
}
