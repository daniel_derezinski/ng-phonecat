import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { IHttpService, IPhone, IPhoneDetail } from '../contracts/phones';
import { environment } from '../../../environments/environment';
import { debounceTime, delay, finalize, map, take, timeout } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpSecondService implements IHttpService {
  private baseUrl = environment.api.baseUrl;

  constructor(private httpClient: HttpClient) {
    console.log('HttpSecondService');
  }

  public getPhoneList(): Observable<IPhone[]> {
    console.warn('url', '/api/phones/phones.json');
    return this.httpClient.get<IPhone[]>(`/api/phones/phones.json`);
  }

  public getPhone(id: string): Observable<any> {
    console.warn('url', `/api/phones/${id}.json`);
    return this.httpClient.get<any>(`/api/phones/${id}.json`);
  }

  public getInstitutions(): Observable<any> {
    console.warn('url', '/etl/institutions/');
    return this.httpClient.get<any>('/etl/institutions/');
  }

}
