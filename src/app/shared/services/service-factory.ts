import { environment } from '../../../environments/environment';
import { HttpService } from './http.service';
import { HttpSecondService } from './http-second.service';
import { HttpClient } from '@angular/common/http';

export function httpServiceFactory(httpClient: HttpClient) {
  if (environment.services.phoneList) {
    return new HttpService(httpClient);
  }
  return new HttpSecondService(httpClient);
}
