import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { IHttpService, IPhone, IPhoneDetail } from '../contracts/phones';
import { environment } from '../../../environments/environment';
import { debounceTime, delay, finalize, map, take, timeout } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService implements IHttpService {
  private baseUrl = environment.api.baseUrl;

  constructor(private httpClient: HttpClient) {
    console.log('HttpService');
  }

  public getPhoneList(): Observable<IPhone[]> {
    return this.httpClient.get<IPhone[]>(`/api/phones/phones.json`)
      .pipe(
        delay(1000)
      );
  }

  public getPhone(id: string): Observable<any> {
    return this.httpClient.get<any>(`/api/phones/${id}.json`)
      .pipe(
        delay(1000)
      );
  }

  public getInstitutions(): Observable<any> {
    return this.httpClient.get<any>('/etl/institutions/')
      .pipe(
        delay(500)
      );
  }

}
