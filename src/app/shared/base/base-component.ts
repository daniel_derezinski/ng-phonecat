import { Subject } from 'rxjs';

export class BaseComponent {

  private _unSubscribe: Subject<void> = new Subject<void>();

  get unSubscribe () {
    return this._unSubscribe;
  }

  ngOnDestroy(): void {
    this._unSubscribe.next();
    this._unSubscribe.complete();
  }
}
