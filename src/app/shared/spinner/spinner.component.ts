import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import * as rootReducers from '../../store/reducers';
import * as rootSelectors from '../../store/selectors';
import { Observable } from 'rxjs';


@Component({
  selector: 'pca-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit {

  showSpinner$: Observable<boolean>;

  constructor(private store: Store<rootReducers.IRootState>) { }

  ngOnInit() {
    this.showSpinner$ = this.store.pipe(select(rootSelectors.selectShowProgressState));
  }

}
