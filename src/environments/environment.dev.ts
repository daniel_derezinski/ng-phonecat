export const environment = {
  production: false,
  api: {
    baseUrl: '/api',
  },
  services: {
    phoneList: false,
  }
};
