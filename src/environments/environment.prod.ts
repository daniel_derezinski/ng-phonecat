export const environment = {
  production: true,
  api: {
    baseUrl: '/api/v1',
  },
  services: {
    phoneList: true,
  }
};
